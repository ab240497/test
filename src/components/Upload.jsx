import { useState } from 'react';
import * as React from 'react';
import { styled } from '@mui/material/styles';
import axios from 'axios';
import { Grid } from '@mui/material';
import Button from '@mui/material/Button';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Link from '@mui/material/Link';
import { NavLink } from 'react-router-dom';

const Input = styled('input')({
  display: 'none',
});

function Upload() {
    const [file, setFile] = useState(null)
  const [uploadedFileData, setUploadedFileData] = useState(null)
  const handleFile = (e) => {
    setFile(e.target.files[0]);
  }
  const handleUpload = () => {
    const formData = new FormData();
    formData.append(
      "file",
      file,
      file.name
    );
    axios.post("http://127.0.0.1:8000/api/fileUpload", formData)
      .then(res => setUploadedFileData(res.data.data))
      .catch(err => alert(err))
  }
  console.log(uploadedFileData);
  const columns = [
    { field: 'file_name', headerName: 'File Name', width: 130 },
    { field: 'file', headerName: 'URL', width: 130 },
  ];
    return (
        <div >
            <Grid>
                <Grid style={{ marginTop: "20px", marginBottom: "20px", marginLeft:"40%" }}>
                
                    <label htmlFor="contained-button-file" style={{ backgroundColor: "#1976D2", color: "#fff", fontSize: "18px", borderRadius: "4px", marginRight: "10px", height: "40px", width: "70px", cursor: "pointer", padding: "8px 12px", marginTop: "10px", marginBottom: "-5px" }}>
                        <Input accept=".zip" size={40} id="contained-button-file" type="file" onChange={handleFile} />
                        Chose File : {file ? file.name : "Click to select"}
                    </label>
                    <Button variant="contained" style={{ height: "40px", marginTop: "-5px" }} onClick={handleUpload}>
                        Upload
                    </Button>
                    <NavLink style={{textDecoration:"none"}} to="/" ><Button style={{color:"#fff",backgroundColor:"#F4591C",marginLeft:"60%", marginTop: "-5px", height: "40px"}}>Back</Button></NavLink>
                </Grid>
                
                <TableContainer component={Paper} style={{ backgroundColor: "#eee", height: "743px" }}>
                    <Table sx={{ minWidth: 650 }} aria-label="simple table">
                        <TableHead style={{ backgroundColor: "#ecf" }}>
                            <TableRow>
                                <TableCell style={{ color: "#6400e1" }}>FILE</TableCell>
                                <TableCell style={{ color: "#6400e1" }} >LINK</TableCell>
                                <TableCell style={{ color: "#6400e1" }} >URL</TableCell>
                                <TableCell style={{ color: "#6400e1" }} >FILE SIZE</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {uploadedFileData ? uploadedFileData.map((row) => (
                                <TableRow
                                    key={row.file_name}
                                >
                                    <TableCell>
                                        {row.file_name}
                                    </TableCell>
                                    <TableCell >
                                        <Link href={row.file} color="inherit">OPEN</Link>
                                    </TableCell>
                                    <TableCell >
                                        {row.file}
                                    </TableCell>
                                    <TableCell >
                                        N/A
                                    </TableCell>
                                </TableRow>
                            )) : null}
                        </TableBody>
                    </Table>
                </TableContainer>
            </Grid>
        </div>
    );
}

export default Upload;