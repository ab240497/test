import React from 'react';
import { Card, Grid, Typography, Button } from "@mui/material";
import { NavLink } from "react-router-dom";

function Home() {
    return (
        <Grid style={{ display: "flex", justifyContent: "space-evenly",textAlign:"center" }}>
            <Card style={{ backgroundColor: "#eee", width: "30%", height: "400px", marginTop: "10%" }}>
                <Typography variant="h2" style={{ textAlign: "center", marginTop: "20px" }}>Video Player</Typography>
                <NavLink style={{textDecoration:"none"}} to="/player"><Button style={{color:"#fff",fontSize:"24px",backgroundColor:"#3189EF",width:"90px",marginTop:"25px"}}>Go</Button></NavLink>
            </Card>
            <Card style={{ backgroundColor: "#eee", width: "30%", height: "400px", marginTop: "10%",textAlign:"center" }}>
                <Typography variant="h2" style={{ textAlign: "center", marginTop: "20px" }}>Upload Zip files</Typography>
                <NavLink style={{textDecoration:"none"}} to="/upload"><Button style={{color:"#fff",fontSize:"24px",backgroundColor:"#3189EF",width:"90px",marginTop:"25px"}}>Go</Button></NavLink>
            </Card>
        </Grid>
    );
}

export default Home;