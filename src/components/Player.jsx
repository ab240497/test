import React, { useEffect, useRef } from "react";
import { useState } from 'react';
import { AppBar, Typography, Toolbar, Container, Card, Grid, Box, CardMedia, IconButton } from '@mui/material';
import { makeStyles } from '@mui/styles';
import ReactPlayer from 'react-player';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import data from './data.json';
import BookmarkIcon from '@mui/icons-material/Bookmark';
import PlayArrowIcon from '@mui/icons-material/PlayArrow';
import PauseIcon from '@mui/icons-material/Pause';
import FullscreenIcon from '@mui/icons-material/Fullscreen';
import VolumeUpIcon from '@mui/icons-material/VolumeUp';
import Slider from '@mui/material/Slider';
import VolumeOffIcon from '@mui/icons-material/VolumeOff';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import CircularProgress from '@mui/material/CircularProgress';
// import FileViewer from 'react-file-viewer';
import screenfull from 'screenfull';
import { NavLink } from "react-router-dom";


const useStyles = makeStyles({
    playerWrapper: {
        width: "100%",
        position: "relative",
    },
    controlWrapper: {
        position: "absolute",
        top: 0,
        left: 0,
        right: 0,
        bottom: 41,
        background: "rgba(0,0,0,0.6)",
        display: "flex",
        flexDirection: "column",
        justifyContent: "space-between",
        zIndex: 1,
    },
    controlIcon: {
        color: "#fff",
        opacity: 0.9,
        "&:hover": {
            transform: "scale(1)",
            color: "#fff"
        }
    }
});

function Player() {
    const classes = useStyles();
    const ref = React.useRef()
    let lessionsList = [...data.videos]
    lessionsList = lessionsList.map(e => ({ ...e, isComplete: false, isplaying: false, played: 0 }))
    const [lists, setLists] = useState(lessionsList)
    const [url, setUrl] = useState(lists[0].sources)
    const [detail, setDetail] = useState(lists[0].description)
    const [title, setTitle] = useState(lists[0].title)
    const [playing, setPlaying] = useState(false)
    const [playingTime, setPlayingTime] = useState("00:00")
    const timeView = (time) => {
        time = Number(time);
        let h = Math.floor(time / 3600);
        let m = Math.floor(time % 3600 / 60);
        let s = Math.floor(time % 3600 % 60);

        let hh = h < 10 ? `0${h}` : h;
        let mm = m < 10 ? `0${m}` : m;
        let ss = s < 10 ? `0${s}` : s;
        if (h > 0) {
            return `${hh}:${mm}:${ss}`
        }
        return `${mm}:${ss}`
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    useEffect(() => {
        let timeCurrent = ref.current.getCurrentTime()
        setPlayingTime(timeCurrent)
    })
    let durTime = timeView(Math.floor(playingTime))
    const [durationTime, setDurationTime] = useState("00:00")
    const [count, setCount] = useState(0)
    const [fileType, setFileType] = useState(false)
    const [loadFile, setLoadFile] = useState("")
    // eslint-disable-next-line no-unused-vars
    const [fileCatagory, setFileCategory] = useState("")
    const handleVideos = (urlSource, description, playedVid, title, type) => {
        setUrl(urlSource)
        setDetail(description)
        setVidPlayed(playedVid)
        setIsReady(false)
        setTitle(title)
        if (type === ".mp4") {
            setFileType(false)
        } else if (type === "pdf") {
            setFileType(true)
            setLoadFile(urlSource)
            setFileCategory(type)
        }
    }
    const handleComplete = () => {
        setLists(lists.map(e => e.sources === url ? ({ ...e, isComplete: true }) : e))
        let currentPlay = lists.find(e => e.sources === url)
        setUrl(lists[currentPlay.id].sources)
        setCount(count + 1)
    }
    const handlePlay = () => {
        setLists(lists.map(e => e.sources === url ? ({ ...e, isplaying: true }) : ({ ...e, isplaying: false })))
    }
    const handleBack = () => {
        let currentPlay = lists.find(e => e.sources === url)
        setUrl(lists[currentPlay.id - 2].sources)
    }
    const handleNext = () => {
        let currentPlay = lists.find(e => e.sources === url)
        setUrl(lists[currentPlay.id].sources)
    }
    const [vidPlayed, setVidPlayed] = useState(0)
    const [isReady, setIsReady] = React.useState(false);
    const onReady = React.useCallback(() => {
        if (!isReady) {
            ref.current.seekTo(vidPlayed, "seconds");
            setIsReady(true);
        }
    }, [isReady, vidPlayed]);
    const handlePause = () => {
        const stamp = ref.current.getCurrentTime()
        setLists(lists.map(e => e.sources === url ? ({ ...e, played: stamp }) : ({ ...e })))
    }
    const handlePlayPause = () => {
        setPlaying(!playing)
    }
    const handleStart = () => {
        let durTime = ref.current.getDuration()
        let min = Math.floor(durTime / 60)
        let sec = Math.floor(durTime % 60)
        let totalDur = `${min}:${sec}`
        setDurationTime(totalDur)
    }

    const [muted, setMuted] = useState(false)
    const handleMuted = () => {
        setMuted(!muted)
    }
    const [volume, setVolume] = useState(70)
    const handleVolumeChange = (vol) => {
        setVolume(vol.target.value)
    }
    const courseComplete = Math.floor((count / lessionsList.length) * 100)
    const playerRef = useRef(null);
    const toggleFullScreen = () => {
        screenfull.toggle(playerRef.current)
    }
    return (
        <>
            <AppBar position="fixed">
                <Toolbar>
                    <Typography variant="h6">React Lessons</Typography>
                    <NavLink style={{textDecoration:"none"}} to="/" ><Button style={{color:"#fff",backgroundColor:"#F4591C",marginLeft:"2200%"}}>Back</Button></NavLink>
                </Toolbar>
            </AppBar>
            <Toolbar />
            <Grid container>
                <Grid xs={8}>
                    <Container maxWidth="md" style={{ height: "70vh" }} >
                         <div ref={playerRef} className={classes.playerWrapper}>
                            <ReactPlayer
                                ref={ref}
                                width="100%"
                                height="100%"
                                url={url}
                                controls={false}
                                playing={playing}
                                onStart={handleStart}
                                onEnded={handleComplete}
                                onPlay={handlePlay}
                                onReady={onReady}
                                onPause={handlePause}
                                muted={muted}
                                volume={volume / 100}
                                config={{
                                    file: {
                                        attributes: {
                                            poster: true,
                                            controlsList: "nofullscreen",
                                        },
                                    },
                                }}
                            />
                            <div className={classes.controlWrapper}>
                                <Grid container direction={"row"} alignItems="center" justifyContent={"space-between"} style={{ padding: "10px" }}>
                                    <Grid item>
                                        <Typography color={"white"}>{title}</Typography>
                                    </Grid>
                                    <Grid item>
                                        <Button
                                            variant="contained"
                                            color="primary"
                                            startIcon={<BookmarkIcon />}
                                        >Bookmark</Button>
                                    </Grid>
                                </Grid>
                                <Grid container direction={"row"} alignItems="center" justifyContent={"space-between"} style={{ padding: "10px" }}>
                                    <Grid item>
                                        <Grid container direction={"row"} alignItems="center" style={{ width: "400px" }}>
                                            <IconButton onClick={handlePlayPause}>
                                                {playing ? <PauseIcon style={{ color: "#fff" }} /> :
                                                    <PlayArrowIcon style={{ color: "#fff" }} />}
                                            </IconButton>
                                            <Typography style={{ color: "#fff" }}>{durTime}/{durationTime}</Typography>
                                            <IconButton onClick={handleMuted}>
                                                {muted ? <VolumeOffIcon style={{ color: "#fff" }} /> :
                                                    <VolumeUpIcon style={{ color: "#fff" }} />}
                                            </IconButton>
                                            <Slider defaultValue={volume} aria-label="Default" valueLabelDisplay="auto" style={{ width: "150px", color: "white" }} onChange={(e) => handleVolumeChange(e)} />
                                        </Grid>
                                    </Grid>
                                    <Grid item>
                                        <Grid container direction={"row"} alignItems="center" justifyContent={"center"}>
                                            <Button type="text" style={{ color: "#fff" }}>1X</Button>
                                            <IconButton onClick={toggleFullScreen}>
                                                <FullscreenIcon fontSize="large" style={{ color: "#fff" }} />
                                            </IconButton>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </div>
                            <Button disabled={url === lists[0].sources} onClick={handleBack}>Back</Button><Button disabled={url === lists[lists.length - 1].sources} onClick={handleNext}>Next</Button>
                        </div> 
                    </Container>
                    <Box>
                        <Typography variant='h5'>
                            Description:
                        </Typography>
                        <Typography sx={{ fontSize: 16 }}>
                            {detail}
                        </Typography>
                    </Box>
                </Grid>
                <Grid xs={4}>
                    <Button type="text" style={{ color: "#78e", width: "98%", height: "7%", backgroundColor: "#000", fontSize: "24px", opacity: 0.8 }}>Progress:
                        <Box sx={{ position: 'relative', display: 'inline-flex', color: "#555", marginLeft: "10px" }}>
                            <CircularProgress style={{ width: "50px" }} sx={{ color: "#eee" }} variant="determinate" value={100} />
                            <CircularProgress
                                variant="determinate"
                                style={{ width: "50px" }}
                                sx={{
                                    position: 'absolute',
                                    left: 0,
                                }}
                                value={courseComplete} />
                            <Box
                                sx={{
                                    top: 0,
                                    left: 15,
                                    bottom: 0,
                                    right: 0,
                                    position: 'absolute',
                                    display: 'flex',
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                }}
                            >
                                <Typography variant="caption" style={{ fontSize: "20px" }} component="div" color="#78e">
                                    {`${courseComplete}%`}
                                </Typography>
                            </Box>
                        </Box>
                    </Button>
                    {lists.map(e => (<Card sx={{ display: 'flex' }} style={{ height: "100px", width: "100%" }} >
                        <Box sx={{ display: 'flex', flexDirection: 'column', width: "80%", height: 100 }}>
                            <CardContent>
                                <Button size="small" onClick={() => handleVideos(e.sources, e.description, e.played, e.title, e.type)}>
                                    <Typography variant="h6" sx={{ fontSize: e.isplaying ? 20 : 16 }} color={e.isplaying ? "blue" : "text.secondary"} gutterBottom>
                                        {e.title}
                                    </Typography>
                                </Button>
                                {e.isComplete && <CheckCircleIcon style={{ color: "green", marginTop: "5px", marginBottom: "-5px", marginLeft: "5px" }} />}
                                <Typography sx={{ fontSize: 14 }}>
                                    {e.subtitle}
                                </Typography>
                            </CardContent>
                        </Box>
                        <CardMedia
                            component="img"
                            sx={{ width: 100, height: 100 }}

                            image={e.thumb}
                            alt="Live"
                        />
                    </Card>))}

                </Grid>
            </Grid>
        </>
    );
}

export default Player;