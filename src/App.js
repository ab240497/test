import React from 'react';
import { Route, Routes } from 'react-router-dom';
import Home from './components/Home';
import Player from './components/Player';
import Upload from './components/Upload';

function App() {
  return (
    <>
    <Routes>
      <Route exact path="/" element={<Home/>} />
      <Route path="/player" element={<Player/>} />
      <Route path="/upload" element={<Upload/>} />
    </Routes>
    </>
  );
}

export default App;
